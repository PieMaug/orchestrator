import yaml

def readYaml(path):
    f = open(path, "r")
    doc = yaml.load(f)
    return doc,f

def getName(doc):
    project_name = getAttribute(doc, 'Name')
    if project_name == "NONE": project_name = 'project'
    return project_name

def getCidr(doc):
    cidr = getAttribute(doc, 'Cidr')
    if cidr == "NONE": cidr = '10.0.1.0/24'
    return cidr

def getImage(doc):
    img = getAttribute(doc, 'Image')
    if img  == "NONE": img = "fedora"
    return img

def getFlavor(doc):
    flv = getAttribute(doc, 'Flavor')
    if flv == "NONE": flv = "m1.small"
    return flv

def getIstancies(doc):
    ists = doc['Istances']
    num = len(ists)
    return ists,num

def getIstanciesNum(ist):
    num = getAttribute(ist, 'istance-num')
    if num == "NONE": num = 1
    return num

def getAttribute(ist, attr):
    try:
        deps = ist[attr]
        return deps
    except KeyError:
        return "NONE"

if __name__ == '__main__':
    doc,f = readYaml("inputfile")
    print getName(doc)
    print getCidr(doc)
    print getImage(doc)
    ists, n = getIstancies(doc)
    print ists[1]
