import keystone
from novaclient import client as nvclient
from novaclient.v2 import shell as nvshell
from neutronclient.v2_0 import client as ntclient
import neutron as neutronManager
import sys
import yaml

if __name__ == '__main__':
	dest_file = open(sys.argv[1], "r")
	doc = yaml.load(dest_file)
	router = doc['router']
	network = doc['net']
	istances = []
	for ist in doc['istances']:
		istances.append(ist)
		dest_file.close()

	neutron = keystone.getClient(ntclient, "network")
	nova = keystone.getClient(nvclient, "compute")
	

	for ist in istances:
		servs = nova.servers.list(search_opts={'name':ist})
		for serv in servs:
			serv.delete()
	
	neutronManager.routerDelete(neutron, router)
	neutronManager.netDelete(neutron, network)


