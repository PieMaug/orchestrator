import json
from neutronclient.v2_0 import client as ntclient
import keystone

# Network creation and deletion
def netCreate(neutron, net_name):
	request_body = { "network": {
						"name": net_name,
						"admin_state_up": True
					}}
	response = neutron.create_network(request_body)
	net_id = response["network"]["id"]
	return net_id

def netDelete(neutron, net_id):
	neutron.delete_network(net_id)
	

# Subnet creation end deletion
def subnetCreate(neutron, net_id, cidr):
	request_body = { "subnet": {
						"network_id": net_id,
						"ip_version": 4,
						"cidr": cidr }}
	response = neutron.create_subnet(request_body);
	subnet_id = response["subnet"]["id"]
	return subnet_id

def subnetDelete(nutron, subnet_id):
	neutron.delete_subnet(subnet_id)


# Router creation and deletion
def routerCreate(neutron, net_id, router_name):
	request_body = { "router": {
						"name": router_name,
						"external_gateway_info": {
							"network_id": net_id},
						"admin_state_up": True}}
	response = neutron.create_router(request_body)
	router_id = response["router"]["id"]
	return router_id

def routerPortsDelete(neutron, router_id):
	response = neutron.list_ports()
	for port in response['ports']:
		if port['device_id'] == router_id:
			request_body = { "port_id" : port['id'] }
			try:
				neutron.remove_interface_router(router_id,
							request_body)
			except:
				a = 1

def routerDelete(neutron, router_id):
	routerPortsDelete(neutron, router_id)
	neutron.delete_router(router_id)

def routerAddIFace(neutron, router_id, subnet_id):
	request_body = { "subnet_id": subnet_id}
	neutron.add_interface_router(router=router_id, body=request_body)


# Get external networks id
def getExtNetwork(neutron):
	response = neutron.list_networks()
	for network in response["networks"]:
		if network["router:external"]:
			return network["id"]

if __name__ == '__main__':

	neutron = keystone.getClient(ntclient, "network")

	extern_net = getExtNetwork(neutron)
	new_net = netCreate(neutron, "project-net")
	new_subnet = subnetCreate(neutron, new_net, "10.0.1.0/24")
	new_router = routerCreate(neutron, extern_net, "project-router")
	routerAddIFace(neutron, new_router, new_subnet)
	
