import yaml
from os.path import basename
import config
from config import getAttribute
from struct import pack


def setUsers():
	doc = "\n#user config\n"
	doc = doc + "password: cloud\n"
	doc = doc + "chpasswd: { expire: False }\n"
	doc = doc + "ssh_pwauth: True\n"
	doc = doc + "lock-passwd: False\n"
	return doc


def setDependencies(deps):
	doc = "\n#dependencies\n"
	doc = doc + "packages:\n"
	for dep in deps:
		doc = doc + " - " + dep + "\n"
	return doc


def setFiles(ist):
        shpaths = getAttribute(ist, 'shfiles')
        gzpaths = getAttribute(ist, 'gzfiles')
	doc = "\n#files\n"
	doc = doc + "write_files:\n"
        if shpaths != "NONE":
	    doc = doc + setShFiles(shpaths)
        if gzpaths != "NONE":
	    doc = doc + setGzFiles(gzpaths) 
	return doc

def setShFiles(paths):
	doc = ""
	for path in paths:
		f = open(path, "r+")
		content = f.read()
		f.close()
		doc = doc + " - path: /" + basename(path) + "\n"
		doc = doc + "   permissions: \'0777\'\n"
		doc = doc + "   content: |\n"
		for line in content.splitlines():
			doc = doc + "         " + line + "\n"
	return doc

def setGzFiles(paths):
	doc = ""
	for path in paths:
		f = open(path, "rb")
		content = f.read()
		f.close()
		doc = doc + " - path: /" + basename(path)[:-3] + "\n"
		doc = doc + "   permissions: \'0777\'\n"
		doc = doc + "   encoding: gzip\n"
		doc = doc + "   content : !!binary |\n"
		for line in content.encode('base64').splitlines():
			doc = doc + "      " + line + "\n"
	return doc

def setActions(ist):
    start_script = getAttribute(ist, 'start-script')
    end_script = getAttribute(ist, 'end-script')
    out_file = getAttribute(ist, 'outfile')
    if start_script == "NONE": raise ValueError("start-script must be specified")
    doc = "\n#Actions\n"
    doc = doc + "runcmd:\n"
    doc = doc + " - sudo /" + start_script
    if end_script != "NONE":
        doc = doc + "\n - sudo /" + end_script
    if out_file != "NONE":
        doc = doc +  " /" + out_file
    doc = doc + "\n"
    return doc

def getUserData(ist):
    document = "#cloud-config\n"
    document = document + setUsers()
    deps = getAttribute(ist, 'dependancies')
    if deps != "NONE":
        document = document + setDependencies(deps)
    document = document + setActions(ist)
    document = document + setFiles(ist)
    return document

if __name__ == '__main__':
	f = open('user-data', 'w')
        doc, ff = config.readYaml('inputfile')
        ists, n = config.getIstancies(doc)
        document = getUserData(ists[1])
	f.write(document)

