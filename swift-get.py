from os import environ as env

to_write = "#!/bin/bash/\n"

to_write = to_write + "AUTH_URL=" + env['OS_AUTH_URL'] + "\n"
to_write = to_write + "USERNAME=" + env['OS_USERNAME'] + "\n"
to_write = to_write + "PASSWORD=" + env['OS_PASSWORD'] + "\n"
to_write = to_write + "TENANT_NAME=" + env['OS_TENANT_NAME'] + "\n"

to_write = to_write + "swift -V 3 --os-auth-url $AUTH_URL --os-username $USERNAME --os-password $PASSWORD --os-tenant-name $TENANT_NAME upload output $1"

f = open("swift-get.sh", "w")
f.write(to_write)
f.close()
