import json
import glanceclient.v2.client as glclient
import keystone


# Get image from name default ubuntu machine
def getImage(glance, image_name):
	images = glance.images.list()
	for image in images:
	    if image_name in image["name"]:
	        return image["id"]
	




if __name__ == '__main__':

	glance = keystone.getClient(glclient, "image")

	print getImage(glance, "cirros")
	print getImage(glance, "fedora")
