import json
import keystone
from novaclient import client as nvclient
from neutronclient.v2_0 import client as ntclient
from glanceclient.v2 import client as glclient
import glance as glanceManager
import neutron as neutronManager
import config 
import userData
import sys

if __name__ == '__main__':
	
        # Read config
        doc,f = config.readYaml(sys.argv[1])
        project_name = config.getName(doc)
        cidr = config.getCidr(doc)
        image_name = config.getImage(doc)
        flavor_name = config.getFlavor(doc)
        istances_conf, istances_num = config.getIstancies(doc)
        for istance in istances_conf:
            userData.getUserData(istance)
        f.close() 

	# Initialize services clients
	neutron = keystone.getClient(ntclient, "network")
	glance = keystone.getClient(glclient, "image")
	nova = keystone.getClient(nvclient, "compute")

	# Get image
	image = glanceManager.getImage(glance, image_name)

	# Create network	
	network	= neutronManager.netCreate(neutron, project_name + "-net")
	subnet = neutronManager.subnetCreate(neutron,network, cidr)
	extern_net = neutronManager.getExtNetwork(neutron)
	router = neutronManager.routerCreate(neutron, extern_net, project_name + "-router")
	neutronManager.routerAddIFace(neutron, router, subnet)

	# Launch istance
        for istance in istances_conf:
            user_data = userData.getUserData(istance)
	    nic = [{ "net-id": network}]
	    for i in range(0,config.getIstanciesNum(istance)):
	        nova.servers.create(name=project_name + "-" + istance['name'],
                                image=image,
				flavor=nova.flavors.find(name=flavor_name),
				nics=nic,
				userdata=user_data)


        #output
        to_write = "router: " + router + "\n"
        to_write = to_write + "net: " + network + "\n"
        to_write = to_write + "istances:\n"
        for istance in istances_conf:
            to_write = to_write + " - " + project_name + "-" + istance['name'] 
        del_file = open(project_name + "-delete", "w")
        del_file.write(to_write)
        del_file.close()

