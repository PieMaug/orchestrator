from os import environ as env
import keystoneclient.v3.client as ksclient
from keystoneauth1 import loading
from keystoneauth1 import session

def keystoneAuth():
	authurl = env["OS_AUTH_URL"] + '/v3'
	keystone = ksclient.Client(auth_url=authurl,
					username=env["OS_USERNAME"],
					password=env["OS_PASSWORD"],
					tenant_name=env["OS_TENANT_NAME"])
	return keystone


def getClient(stack_client, serv_type):
	keystone = keystoneAuth()
	endpoint = keystone.service_catalog.url_for(service_type=serv_type)
	client = ""
	if serv_type == "network":
		client = stack_client.Client(endpoint_url=endpoint,
								token=keystone.auth_token)
	elif serv_type == "image":
		client = stack_client.Client(endpoint,
									token=keystone.auth_token)
	elif serv_type == "compute":
		loader = loading.get_plugin_loader('password')
		auth = loader.load_from_options(
								auth_url=env["OS_AUTH_URL"] + '/v3',
								username=env["OS_USERNAME"],
								password=env["OS_PASSWORD"],
								project_name=env["OS_TENANT_NAME"],
						user_domain_name=env["OS_USER_DOMAIN_ID"],
						project_domain_id=env["OS_PROJECT_DOMAIN_ID"])
		sess = session.Session(auth=auth)
		client = stack_client.Client(2.1, session=sess)
	return client
